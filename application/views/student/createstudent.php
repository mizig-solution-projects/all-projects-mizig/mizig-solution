<?php include "bootstrap3.php" ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>adding student</title>
</head>

<body>
  <?php echo form_open('student/createstudent'); ?>
  <h1>student list</h1>
  <div>
    <h5>student</h5>
    <input type="text" name="student" placeholder="student name please" size="50" />
    <h5>email</h5>
    <input type="email" name="email" placeholder="emailplease" size="50" />
    <h5>phone</h5>
    <input type="phone" name="phone" placeholder="phone number please" size="50" />
  </div>
  <h5>class</h5>
  <select class="form-control" name="class">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
  </select>
  <button type="submit" class="btn btn-default">Submit</button>
  </form>
</body>

</html>