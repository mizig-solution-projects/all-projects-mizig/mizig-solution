
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Teacher extends CI_Controller
{
  public function index()
  {
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email ID', 'required');
    $this->form_validation->set_rules('name', 'name', 'required');
    $this->form_validation->set_rules('password', 'password', 'required');
    if ($this->form_validation->run() == FALSE) {
      $this->load->view('teacher/teachersview');
    } else {
      $name = $this->input->post('name');
      $this->load->model('admin_model');
      $data['data'] = $this->admin_model->alldetails($name);
      if ($data != NULL) {
        $this->load->view('teacher/alldetails', $data, $name);
      } else {
        echo "no data found";
      }
    }
  }
}
