<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student extends CI_Controller
{
  public function index()
  {
    $this->load->model('admin_model');
    $result['data'] = $this->admin_model->studentdisplay();
    $this->load->view('student/display_student', $result);
  }
  public function createstudent()
  {
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'email', 'required');
    $this->form_validation->set_rules('phone', 'phone', 'required');
    $this->form_validation->set_rules('class', 'class', 'required');
    $this->form_validation->set_rules('student', 'student', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('student/createstudent');
    } else {
      $this->load->model('admin_model');
      $data = array();
      $data['class'] = $this->input->post('class');
      $data['email'] = $this->input->post('email');
      $data['phone'] = $this->input->post('phone');
      $data['student'] = $this->input->post('student');
      if ($data > 0) {
        $this->admin_model->studentinsert($data);
        echo "created successfull";
      } else {
        echo "not inserted";
      }
    }
  }
  public function searchstudent()
  {
    $this->load->view('student/searchstudent');
  }
  public function displaysearch()
  {
    $this->load->model('admin_model');
    $class = $this->input->post('class');
    $result['data'] = $this->admin_model->student_displaysearch($class);
    $this->load->view('student/displaystudentsearch', $result, $class);
  }
}
