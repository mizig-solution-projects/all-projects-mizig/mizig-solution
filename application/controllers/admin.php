<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	public function index()
	{jnfiurirbi
		hello world
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email ID', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		if ($this->form_validation->run() == false) {
			$this->load->view('login_view');
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$this->load->model('admin_model');
			$result = $this->admin_model->checkLogin($email, $password);
			if ($result != NULL) {
				$this->load->view('admin_view/pages');
			} else {
				echo "incorrect email and password";
			}
		}
	}
	public function teacherspage()
	{
		$this->load->model('admin_model');
		$result['data'] = $this->admin_model->teacherdisplay();
		$this->load->view('admin_view/teachersview', $result);
	}
	public function createteacher()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'Email ID', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('admin_view/createteacher');
		} else {
			$this->load->model('admin_model');
			$data = array();
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			$data['password'] = $this->input->post('password');
			if ($data != NULL) {
				$this->admin_model->teacher_insert($data);
				echo "created successfull";
			} else {
				echo "not inserted";
			}
		}
	}
	public function deleterecords()
	{
		$this->load->model('admin_model');
		$id = $this->input->get('id');
		$responce = $this->admin_model->delete_records($id);
		if ($responce == true) {
			echo "success";
		} else {
			echo "not success";
		}
	}
	public function assignwork()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email ID', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('admin_view/teacherwork');
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$this->load->model('admin_model');
			$result = $this->admin_model->checkteacher($email, $password);
			if ($result!=NULL) {
				$this->load->view('admin_view/assign_workview');
			} else {
				echo "invalid email";
			}
		}
	}
	public function worksave()
	{
		$this->load->model('admin_model');
		$data = array();
		$data['name'] = $this->input->post('name');
		$data['class'] = $this->input->post('class');
		$data['subjects'] = $this->input->post('subjects');
		if ($data != NULL) {
			$this->admin_model->teacherswork($data);
			echo "created successfull";
		} else {
			echo "not inserted";
		}
	}
	public function display()
	{
		$this->load->view("admin_view/display_teacherwork");
		$this->load->model('admin_model');
		$data= $this->admin_model->teachersworkdisplay();
		 echo json_encode($data);
	}
}
