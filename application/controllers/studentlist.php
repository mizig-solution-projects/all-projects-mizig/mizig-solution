<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Studentlist extends CI_Controller
{ 
    public function index()
    {
        $this->load->model('admin_model');
        $result['data']=$this->admin_model->studentdisplay();
        $this->load->view('student/studentlist',$result);
    }
}
?>