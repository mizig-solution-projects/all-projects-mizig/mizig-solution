<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{
  public function index()
  {
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'name', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('teacher/profilepage');
    } else {
      $name = $this->input->post('name');
      $this->load->model('admin_model');
      $result['data'] = $this->admin_model->checkprofile($name);
      if ($result['data'] != NULL) {
        $this->load->view('teacher/profiledetails', $result);
      } else {
        echo "not found";
      }
    }
  }
  public function update()
  {
      $id = $this->input->get('id');
      echo $id;
      $this->load->model('admin_model');
            $result['data']=$this->admin_model->display_ById($id);
      $this->load->view('update',$result);
  }

  public function updatedata()
  {
    $id = $this->input->get('id');
    $this->load->model('admin_model');
    //using associative arrays
			$data = array();
			$data['name'] = $this->input->post('name');
			$data['email'] = $this->input->post('email');
			if ($data != NULL) {
				$this->admin_model->update($id,$data);
				echo "created successfull";
			} else {
				echo "not inserted";
			}
		}
    
}
