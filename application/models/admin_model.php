<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_Model extends CI_Model
{
	public function checkLogin($email, $password)
	{
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$query = $this->db->get('tbladmin');
		return $query->result();
	}
	function display_ById($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('teacherslogin');
		return $query->row();
	
	}
	public function teacherdisplay()
	{
		$query = $this->db->query('SELECT * FROM teacherslogin');
		return $query->result();
	}
	public function display()
	{
		$query = $this->db->query('SELECT * FROM subjects');
		return $query->result();
	}
	public function teacher_insert($data)
	{
		$this->db->insert('teacherslogin', $data);
	}
	public function checkteacher($email, $password)
	{
		$this->db->where('email', $email);
		$this->db->where('password', $password);
		$query = $this->db->get('teacherslogin');
		return $query->result();
	}
	public function checkprofile($name)
	{
		$query = $this->db->query("SELECT * FROM teacherslogin WHERE name='$name'");
		return $query->result();
	}
	 
	public function delete_records($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('teacherslogin');
		return true;
	}
	public function teacherswork($data)
	{
		$this->db->insert('teacherswork', $data);
	}
	public function teachersworkdisplay()
	{
		$query = $this->db->query('SELECT * FROM teacherswork');
		return $query->result();
	}
	public function studentinsert($data)
	{
		$this->db->insert('studentlist', $data);
	}
	public function studentdisplay()
	{
		$query = $this->db->query('SELECT * FROM studentlist');
		return $query->result();
	}
	public function student_displaysearch($class)
	{
		$this->db->select('*');
		$this->db->from('studentlist');
		$this->db->where('class', $class);
		return $this->db->get()->result_array();
	}
	public function alldetails($name)
	{
		$this->db->select('teacherslogin.id,teacherslogin.email,teacherswork.name,teacherswork.class,teacherswork.subjects');
		$this->db->from('teacherslogin');
		$this->db->join('teacherswork', 'teacherslogin.id = teacherswork.class',);
		$this->db->where('teacherswork.name', $name);
		return $this->db->get()->result_array();
	}
	function display_records()
	{
		$query = $this->db->query('SELECT * FROM teacherslogin');
		return $query->result();
	}
	function update($id,$data)
	{
		$this->db->where('id', $id);
		$this->db->update('teacherslogin', $data);
	}
}
