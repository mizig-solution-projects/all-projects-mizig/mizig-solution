-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 30, 2022 at 08:58 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school1`
--

-- --------------------------------------------------------

--
-- Table structure for table `studentlist`
--

CREATE TABLE `studentlist` (
  `id` int(50) NOT NULL,
  `class` varchar(50) NOT NULL,
  `student` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `studentlist`
--

INSERT INTO `studentlist` (`id`, `class`, `student`, `email`, `phone`) VALUES
(1, '1', 'ram', 'ram@gmail.com', '587496459'),
(2, '2', 'shaam', 'shaam@gmail.com', '7578645156'),
(3, '3', 'soma', 'soma@gmail.com', '754685126'),
(4, '4', 'soraj', 'soraj@gmail.com', '4569876'),
(5, '5', 'hidayath', 'hidayat@gmail.com', '74569845'),
(6, '6', 'suhan', 'suhan@gmail.com', '456812365'),
(17, '5', 'suresh', 'suresh@gmail.com', '745869565'),
(18, '5', 'drona', 'drona@gmail.com', '43817828'),
(19, '5', 'shaam', 'shaam@gmail.com', '7795762083');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(5) NOT NULL,
  `language` varchar(10) NOT NULL,
  `teachers` varchar(30) DEFAULT NULL,
  `class` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `language`, `teachers`, `class`) VALUES
(1, 'hindi', 'suresh', 1),
(2, 'english', 'mahesh', 2),
(4, 'mathe', 'hidayath', 4),
(5, 'science', 'suhan', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `id` int(11) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`id`, `email`, `password`) VALUES
(1, 'admin@gmail.com', 'Test@123');

-- --------------------------------------------------------

--
-- Table structure for table `teacherslogin`
--

CREATE TABLE `teacherslogin` (
  `id` int(40) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teacherslogin`
--

INSERT INTO `teacherslogin` (`id`, `name`, `email`, `password`) VALUES
(2, 'gagan2', 'gagan@gmail.com', 123),
(3, 'hidayath', 'hidayathpashackm@gmail.com', 123),
(11, 'suhan', 'suhan@gmail.com', 1243);

-- --------------------------------------------------------

--
-- Table structure for table `teacherswork`
--

CREATE TABLE `teacherswork` (
  `id` int(50) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `class` int(40) DEFAULT NULL,
  `subjects` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teacherswork`
--

INSERT INTO `teacherswork` (`id`, `name`, `class`, `subjects`) VALUES
(1, 'hidayath', 2, 'social'),
(2, 'suresh', 3, 'mathe'),
(3, 'suhan', 11, 'science');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `studentlist`
--
ALTER TABLE `studentlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacherslogin`
--
ALTER TABLE `teacherslogin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacherswork`
--
ALTER TABLE `teacherswork`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class` (`class`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `studentlist`
--
ALTER TABLE `studentlist`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `teacherslogin`
--
ALTER TABLE `teacherslogin`
  MODIFY `id` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `teacherswork`
--
ALTER TABLE `teacherswork`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `teacherswork`
--
ALTER TABLE `teacherswork`
  ADD CONSTRAINT `teacherswork_ibfk_1` FOREIGN KEY (`class`) REFERENCES `teacherslogin` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
